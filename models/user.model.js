const mongoose = require("mongoose");
const { isEmail } = require('validator');
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minLength: 3,
        maxLength: 55,
        unique: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        validate: [isEmail],
        lowercase: true,
        unique: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
        max: 1024,
        minLength: 6
    },
    firstName: {
        type: String,  
    },
    lastName: {
        type: String,  
    },
    telephone: {
        type: String,
        maxLength: 15
    },
    city: {
        type: String,
    },
    zipCode: {
        type: String,
        maxLength: 5
    },
    userConnected: {
        type: Boolean,
        default: false
    },
    lastLogin: {
        type: Date,
        default: Date.now()
    },
    userTypes: {
        type: String,
        enum: ['farmer', 'gardener', 'giver', 'lender', 'borrower', 'receiver'],
        default: 'farmer'
    }
    // userTypes: [
    //     {
    //         type: mongoose.Types.ObjectId,
    //         ref: "UserType"
    //     }
    // ],

}, { timestamps: true });

userSchema.pre("save", async function(next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

userSchema.statics.login = async function(username, password) {
    const user = await this.findOne({ username });
    if (user) {
        const auth = await bcrypt.compare(password, user.password);
        if (auth) {
            return user;
        }
        throw Error('Incorrect password');
    }
    throw Error('Incorrect username');
};

const UserModel = mongoose.model("User", userSchema);

module.exports = UserModel;