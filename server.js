const express = require('express');
const bodyParser = require('body-parser');

// import user routes
const userRoutes = require('./routes/user.routes');
require('dotenv').config({ path: 'config/.env'});
require('./config/db.config');
const app = express();

// setup the server port
const port = process.env.PORT || 5002;

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// define root route
app.get('/', (req, res) => {
    res.send('Hello NODEJS');
});

// create user routes
app.use('/api/user', userRoutes)

// set port, listen for requests
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
})