const mongoose = require('mongoose');
mongoose.connect("mongodb+srv://" + process.env.DB_USER_PASS + "@nodetuts-ninja.7caiu.mongodb.net/agro-interest", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => console.log('Connected to MongoDb')).catch((err) => console.log('Failed to connect to MongoDB'));