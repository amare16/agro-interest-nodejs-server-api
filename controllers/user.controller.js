const UserModel = require('../models/user.model');
const ObjectId = require("mongoose").Types.ObjectId;

const getAllUsers = async (req, res) => {
    const user = await UserModel.find().select('-password');
    res.status(200).json({ user });
};

const getSingleUser = async (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send("Unknown ID : " + req.params.id);
    UserModel.findById(req.params.id, (err, docs) => {
        if (!err) res.send(docs);
        else console.log("ID unknow: " + err);
    }).select("-password");
}

module.exports = {
    getAllUsers,
    getSingleUser
};