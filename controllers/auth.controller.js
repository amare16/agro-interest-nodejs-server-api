const UserModel = require("../models/user.model");
const jwt = require("jsonwebtoken");
const { signUpErrors, signInErrors } = require("../utils/errors.utils");

const maxAge = 3 * 24 * 60 * 60 * 1000;
const createToken = (id) => {
  return jwt.sign({id}, process.env.TOKEN_SECRET, {
    expiresIn: maxAge
  });
}

module.exports.signUp = async (req, res) => {
      const {
        username,
        email,
        password,
        firstName,
        lastName,
        telephone,
        city,
        zipCode,
        userTypes,
      } = req.body;
    try {
      const newUser = new UserModel({ username, email, password, firstName, lastName, telephone, city, zipCode, userTypes });
      await newUser.save();
        res.json({
            data: newUser
        });
    } catch (err) {
      const errors = signUpErrors(err);
        res.status(200).send({ errors })
    }
  
};

module.exports.signIn = async (req, res) => {
  const { username, password } = req.body;
  
  try {
    const user = await UserModel.login(username, password);

    const token = createToken(user._id);
    console.log("tok:", token);
    res.cookie('jwt', token, { httpOnly: true, maxAge });
    res.status(200).json({ user: user._id });
  } catch (err) {
    const errors = signInErrors(err);
    res.status(200).send({ errors });
  }
}

module.exports.signOut = async (req, res) => {
  res.cookie('jwt', '', { maxAge: 1 });
  res.redirect('/');
  console.log('Logged out already!');
}
