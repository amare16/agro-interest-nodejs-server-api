const signUpErrors = (err) => {
    let errors = { username: '', email: '', password: '' };

    if (err.message.includes('username'))
        errors.username = "Incorrect Username";
    
    if (err.message.includes('email'))
        errors.email = "Incorrect Email";

    if (err.message.includes('password'))
        errors.password = "Password must be at least 6 characters";
    
    if (err.code === 11000 && Object.keys(err.keyValue)[0].includes('username'))
        errors.username = "Username already registered";
    if (err.code === 11000 && Object.keys(err.keyValue)[0].includes('email'))
        errors.email = "Email already registered";
    return errors;
};

const signInErrors = (err) => {
    let errors = { username: '', password: ''};

    if (err.message.includes('username'))
        errors.username = "Unknown username";
    
    if (err.message.includes('password'))
        errors.password = "Password does not match";

    return errors;
}


module.exports = { signUpErrors, signInErrors };
