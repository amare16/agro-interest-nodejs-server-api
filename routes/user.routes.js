const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth.controller');
const userController = require('../controllers/user.controller');
// create new user
router.post('/add-user', authController.signUp);

// Login
router.post('/login', authController.signIn);

// Logout
router.get("/logout", authController.signOut);

// Get all users
router.get('/', userController.getAllUsers);

// Get single user
router.get('/:id', userController.getSingleUser);



module.exports = router;